
function defaults(obj,defaultprops) {
    return {...obj,...defaultprops}
}

module.exports = defaults;