function mapObject(obj,callback) {
    const output = {};
    for(let key in obj) {
        if(typeof obj[key] != "number") {
            output[key] = obj[key];
        } else{
            output[key] = callback(obj[key]);
        }
    }
    return output;
}

module.exports = mapObject;